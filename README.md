FSFE Artwork Meta Repository
============================

This git repository contains artwork created and used by Free Software Foundation Europe.

Work Flow
---------

1. Check out repository: `git clone https://gitlab.com/fsfe/artwork.git`
2. Initialize sub-repositories: `git submodule init`
3. Update sub-repositories: `git submodule update`

Whenever you want to update to the latest version run `./update.sh`

1. Review your changes: `git status`
2. Commit all your changes: `git commit -a`
3. Push your changes to the main repository: `git push origin master`

